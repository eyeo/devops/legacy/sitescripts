# This file is part of the Adblock Plus web scripts,
# Copyright (C) 2006-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import re
import subprocess
import tempfile
import shutil
import zipfile
from StringIO import StringIO
from ...utils import get_config, setupStderr
from ..combineSubscriptions import combine_subscriptions, FileSource


class MercurialSource:
    _prefix = ''

    def __init__(self, repo):
        print 'MercurialSource init repo: ', repo
        command = ['hg', '-R', repo, 'archive', '-r', 'default',
                   '-t', 'uzip', '-p', '.', '-']
        data = subprocess.check_output(command)
        self._archive = zipfile.ZipFile(StringIO(data), mode='r')

    def close(self):
        self._archive.close()

    def read_file(self, filename):
        return self._archive.read(self._prefix + filename).decode('utf-8')

    def list_top_level_files(self):
        for filename in self._archive.namelist():
            if filename.startswith(self._prefix):
                filename = filename[len(self._prefix):]
            if '/' not in filename:
                print 'list_top_level_files: ', filename
                yield filename


if __name__ == '__main__':
    setupStderr()

    source_repos = {}
    for option, value in get_config().items('subscriptionDownloads'):
        if option.endswith('_repository'):
            source_repos[re.sub(r'_repository$', '', option)] = MercurialSource(value)
        elif option.endswith('_directory'):
            source_repos[re.sub(r'_directory$', '', option)] = FileSource(value)

    basedir = get_config().get('subscriptionDownloads', 'outdir')
    destination = os.path.join(basedir, 'data')
    try:
        combine_subscriptions(source_repos, destination, tempdir=basedir)
    except Exception:
        print >>sys.stderr, 'Failed to combine subscriptions of source repos: {}.'.format(source_repos)
        print >>sys.stderr, 'Was using destination: {} tempdir: {}'.format(destination, source_repos)
    finally:
        for source in source_repos.itervalues():
            source.close()
